package com.example.aftbhadk.myexpence.Interfaces;

import android.widget.ImageView;

import com.example.aftbhadk.myexpence.DataBase.Entity.ExpenceEntity;

public interface MainActivityInterface {

    void showPopupmenu(ExpenceEntity expenceEntity, ImageView imageView, int position);

    void itemClickListener(int position, ExpenceEntity expenceEntity);
}
