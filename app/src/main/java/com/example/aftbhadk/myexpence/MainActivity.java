package com.example.aftbhadk.myexpence;

import android.annotation.TargetApi;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.aftbhadk.myexpence.Adapters.CategoriesListAdapter;
import com.example.aftbhadk.myexpence.Adapters.ExpenceAdapter;
import com.example.aftbhadk.myexpence.DataBase.Entity.CategoriesEntity;
import com.example.aftbhadk.myexpence.DataBase.Entity.ExpenceEntity;
import com.example.aftbhadk.myexpence.Interfaces.MainActivityInterface;
import com.example.aftbhadk.myexpence.Models.ExpenceModel;
import com.example.aftbhadk.myexpence.ViewModels.MainActivityViewModel;
import com.example.aftbhadk.myexpence.Views.AddExpenceDetails;
import com.example.aftbhadk.myexpence.Views.CategoryListActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainActivityInterface {

    RecyclerView expenceList;
    MainActivityViewModel mainActivityViewModel;
    ExpenceAdapter expenceAdapter;
    Button emaddExpence, errorBt;
    TextView totalExpence;
    private int totalamount;
    FloatingActionButton action_add;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mainActivityViewModel = ViewModelProviders.of(MainActivity.this).get(MainActivityViewModel.class);

        initViews();
    }

    private void initViews() {

        totalExpence = (TextView) findViewById(R.id.totalExpence);
        emaddExpence = (Button) findViewById(R.id.emaddExpence);
        errorBt = (Button) findViewById(R.id.erroeBt);
        action_add = (FloatingActionButton) findViewById(R.id.action_add);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        emaddExpence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AddExpenceDetails.class));
            }
        });

        action_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AddExpenceDetails.class));
            }
        });

        expenceList = (RecyclerView) findViewById(R.id.expenceList);

        expenceList.setLayoutManager(new LinearLayoutManager(this));
        expenceAdapter = new ExpenceAdapter(this, new ArrayList<ExpenceEntity>(), this);
        expenceList.setAdapter(expenceAdapter);
        mainActivityViewModel.getListLiveData().observe(MainActivity.this, new Observer<List<ExpenceEntity>>() {
            @Override
            public void onChanged(@Nullable List<ExpenceEntity> categoriesEntities) {
                expenceAdapter.addItems(categoriesEntities);
                totalExpencecount(categoriesEntities);
                if (categoriesEntities.size() == 0) {
                    errorBt.setVisibility(View.VISIBLE);
                } else {
                    errorBt.setVisibility(View.GONE);
                }
            }
        });
    }

    private void totalExpencecount(List<ExpenceEntity> categoriesEntities) {
        totalamount = 0;
        for (ExpenceEntity expenceEntity : categoriesEntities) {
            totalamount = totalamount + Integer.valueOf(expenceEntity.getAmount());
        }
        totalExpence.setText("Total Expence \n" + totalamount + " Rs");
        getSupportActionBar().setSubtitle("Total " + totalamount + " Rs");


    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void showPopupmenu(final ExpenceEntity expenceEntity, ImageView imageView, final int position) {

        PopupMenu popupMenu = new PopupMenu(MainActivity.this, imageView);
        popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete:
                        mainActivityViewModel.deleteSingleExpence(expenceEntity);
                        //expenceAdapter.notifyItemRemoved(position);
                        break;
                    case R.id.edit:
                        Log.d("Edit", expenceEntity.getAmount());
                        break;

                }
                return true;
            }
        });
        popupMenu.show();

    }

    @Override
    public void itemClickListener(int position, ExpenceEntity expenceEntity) {

    }

//    private void gettingData() {
//        expenceModelList.add(new ExpenceModel(1, "Monday 5/15/2018", "Eating Out", R.drawable.eating, "500"));
//        expenceModelList.add(new ExpenceModel(1, "Monday 5/15/2018", "Entertainment", R.drawable.entertainment, "500"));
//        expenceModelList.add(new ExpenceModel(1, "Monday 5/15/2018", "Grocery", R.drawable.grocery, "500"));
//        expenceModelList.add(new ExpenceModel(1, "Monday 5/15/2018", "Petrol", R.drawable.petrol, "500"));
//        expenceModelList.add(new ExpenceModel(1, "Monday 5/15/2018", "Shoping", R.drawable.shoping, "500"));
//        expenceModelList.add(new ExpenceModel(1, "Monday 5/15/2018", "Travel", R.drawable.travel, "500"));
//    }
}
