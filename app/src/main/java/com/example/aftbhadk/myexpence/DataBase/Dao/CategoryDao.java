package com.example.aftbhadk.myexpence.DataBase.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.aftbhadk.myexpence.DataBase.Entity.CategoriesEntity;

import java.util.List;

@Dao
public interface CategoryDao {

    @Query("SELECT * FROM categories")
    LiveData<List<CategoriesEntity>> getallCategories();

    @Insert
    void insertCategories(CategoriesEntity... categoriesEntities);

    @Query("SELECT * FROM categories WHERE id = :id LIMIT 1")
    CategoriesEntity getCategoryFromID(int id);




}
