package com.example.aftbhadk.myexpence;

import android.app.DatePickerDialog;
import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.widget.DatePicker;

import com.example.aftbhadk.myexpence.Interfaces.AddExpenceInterface;
import com.example.aftbhadk.myexpence.Views.AddExpenceDetails;

import java.util.Calendar;


public class AppUtility {

    /**
     * @param context
     */
    public static void showDatePicker(final Context context, final AddExpenceInterface addExpenceInterface) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        //editText.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        addExpenceInterface.getDate(dayOfMonth , monthOfYear + 1, year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
}
