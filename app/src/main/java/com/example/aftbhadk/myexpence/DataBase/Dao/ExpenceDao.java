package com.example.aftbhadk.myexpence.DataBase.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.aftbhadk.myexpence.DataBase.Entity.ExpenceEntity;

import java.util.List;

@Dao
public interface ExpenceDao {
    @Query("SELECT * FROM expences")
    LiveData<List<ExpenceEntity>> getAllExpences();

    @Insert
    void addExpence(ExpenceEntity... expenceEntities);

    @Delete
    void deleteSingleExpence(ExpenceEntity... expenceEntities);


}
