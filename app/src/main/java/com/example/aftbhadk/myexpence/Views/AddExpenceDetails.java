package com.example.aftbhadk.myexpence.Views;

import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import com.example.aftbhadk.myexpence.AppUtility;
import com.example.aftbhadk.myexpence.DataBase.Entity.ExpenceEntity;
import com.example.aftbhadk.myexpence.Interfaces.AddExpenceInterface;
import com.example.aftbhadk.myexpence.R;
import com.example.aftbhadk.myexpence.ViewModels.MainActivityViewModel;

import java.util.Calendar;

public class AddExpenceDetails extends AppCompatActivity implements AddExpenceInterface {

    private TextInputEditText pickDate;
    private TextInputEditText category;
    private TextInputEditText amount;
    private TextInputEditText notes;
    private Button addDetails;
    MainActivityViewModel mainActivityViewModel;
    private int date = 0, month = 0, year = 0;
    private String eCategory = null;
    private int imageValue = 0;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expence_details);

        initViews();
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pickDate = (TextInputEditText) findViewById(R.id.etDate);
        category = (TextInputEditText) findViewById(R.id.etCategory);
        amount = (TextInputEditText) findViewById(R.id.amount);
        notes = (TextInputEditText) findViewById(R.id.notes);
        addDetails = (Button) findViewById(R.id.addDetails);
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        addDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveDetails();

            }
        });
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddExpenceDetails.this, CategoryListActivity.class);
                startActivityForResult(intent, 100);
            }
        });
        category.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Intent intent = new Intent(AddExpenceDetails.this, CategoryListActivity.class);
                    startActivityForResult(intent, 100);
                }
            }
        });
        pickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtility.showDatePicker(AddExpenceDetails.this, AddExpenceDetails.this);
            }
        });
        pickDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    AppUtility.showDatePicker(AddExpenceDetails.this, AddExpenceDetails.this);
                }
            }
        });

    }

    private void saveDetails() {

        if (date != 0 && month != 0 && year != 0 &&
                amount.getText().toString() != null &&
                notes.getText().toString() != null &&
                category.getText().toString() != null &&
                eCategory != null &&
                imageValue != 0) {

            ExpenceEntity expenceEntity = new ExpenceEntity();
            expenceEntity.setAmount(amount.getText().toString());
            expenceEntity.setDate(date);
            expenceEntity.setMonth(month);
            expenceEntity.setYear(year);
            expenceEntity.setCategoryid(eCategory);
            expenceEntity.setNotes(notes.getText().toString());
            expenceEntity.setImage(imageValue);
            mainActivityViewModel.setListLiveData(expenceEntity);
            finish();

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("RESULT", resultCode + "");
        if (requestCode == 100 && resultCode == 100) {
            category.setText(data.getStringExtra("CATEGORY"));
            eCategory = data.getStringExtra("CATEGORY");
            imageValue = data.getIntExtra("CATEGORYID", 0);
        }
    }

    @Override
    public void getDate(int date, int month, int year) {
        this.date = date;
        this.month = month;
        this.year = year;
        pickDate.setText(date + "/" + month
                + "/" + year);
    }
}
