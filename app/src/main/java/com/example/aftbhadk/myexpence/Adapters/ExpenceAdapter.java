package com.example.aftbhadk.myexpence.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.aftbhadk.myexpence.DataBase.Entity.CategoriesEntity;
import com.example.aftbhadk.myexpence.DataBase.Entity.ExpenceEntity;
import com.example.aftbhadk.myexpence.Interfaces.MainActivityInterface;
import com.example.aftbhadk.myexpence.Models.ExpenceModel;
import com.example.aftbhadk.myexpence.R;

import java.util.List;

public class ExpenceAdapter extends RecyclerView.Adapter<ExpenceAdapter.MyViewHolder> {

    Context context;
    List<ExpenceEntity> expenceList;
    MainActivityInterface mainActivityInterface;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expence_row, parent, false);

        return new MyViewHolder(itemView);

    }

    public ExpenceAdapter(Context context, List<ExpenceEntity> expenceList, MainActivityInterface mainActivityInterface) {
        this.context = context;
        this.expenceList = expenceList;
        this.mainActivityInterface = mainActivityInterface;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final ExpenceEntity expenceEntity = expenceList.get(position);
        holder.emTitle.setText(expenceEntity.getCategoryid());
        holder.emDate.setText(expenceEntity.getDate() + "/" + expenceEntity.getMonth() + "/" + expenceEntity.getYear());
        holder.emPrice.setText(expenceEntity.getAmount() + " Rs");
        holder.emIcon.setImageResource(expenceEntity.getImage());
        holder.notes.setText(expenceEntity.getNotes());
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivityInterface.itemClickListener(position, expenceEntity);
            }
        });

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mainActivityInterface.showPopupmenu(expenceEntity, holder.more, position);

            }
        });
        holder.more.setOnClickListener(v -> {
            mainActivityInterface.showPopupmenu(expenceEntity, holder.more, position);
        });


    }

    public void addItems(List<ExpenceEntity> expenceList) {
        this.expenceList = expenceList;

    }

    @Override
    public int getItemCount() {
        return expenceList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView emTitle;
        private TextView emDate;
        private TextView emPrice;
        private ImageView emIcon, more;
        private TextView notes;
        private RelativeLayout relativeLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            emTitle = (TextView) itemView.findViewById(R.id.emTitle);
            emDate = (TextView) itemView.findViewById(R.id.emDate);
            emPrice = (TextView) itemView.findViewById(R.id.emPrice);
            emIcon = (ImageView) itemView.findViewById(R.id.emIcon);
            notes = (TextView) itemView.findViewById(R.id.emNotes);
            more = (ImageView) itemView.findViewById(R.id.more);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.holder);
        }

    }
}
