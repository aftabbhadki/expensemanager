package com.example.aftbhadk.myexpence.Views;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.example.aftbhadk.myexpence.Adapters.CategoriesListAdapter;
import com.example.aftbhadk.myexpence.DataBase.Entity.CategoriesEntity;
import com.example.aftbhadk.myexpence.Interfaces.OnListItemClick;
import com.example.aftbhadk.myexpence.R;
import com.example.aftbhadk.myexpence.ViewModels.CategoryListViewModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryListActivity extends AppCompatActivity implements OnListItemClick {

    RecyclerView recyclerView;
    CategoryListViewModel categoryListViewModel;
    CategoriesListAdapter categoriesListAdapter;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);

        initViews();

    }

    private void initViews() {
        categoryListViewModel = ViewModelProviders.of(this).get(CategoryListViewModel.class);
        recyclerView = (RecyclerView) findViewById(R.id.categoryList);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        categoriesListAdapter = new CategoriesListAdapter(this, new ArrayList<CategoriesEntity>(), this);
        recyclerView.setAdapter(categoriesListAdapter);
        categoryListViewModel.getCategoriesEntityList().observe(CategoryListActivity.this, new Observer<List<CategoriesEntity>>() {
            @Override
            public void onChanged(@Nullable List<CategoriesEntity> categoriesEntities) {
                categoriesListAdapter.addItems(categoriesEntities);
                if (categoriesEntities.size() == 0) {
                    setDummyData();
                }
            }
        });


    }

    private void setDummyData() {

        String[] categoriesname = {"Eating Out", "Petrol", "Travel", "Entertainment", "Grocery", "Shopping", "Mobile Recharge", "Credit Card", "Other"};
        int[] imagesName = {R.drawable.eating, R.drawable.petrol, R.drawable.travel, R.drawable.entertainment, R.drawable.grocery, R.drawable.shoping, R.drawable.smartphone, R.drawable.credit, R.drawable.other};
        for (int i = 0; i < categoriesname.length; i++) {
            CategoriesEntity categoriesEntity = new CategoriesEntity(categoriesname[i], imagesName[i]);
            categoryListViewModel.setCategoriesEntityList(categoriesEntity);
        }
    }

    @Override
    public void OnClickItem(int position, int id) {
        CategoriesEntity categoriesEntity = categoryListViewModel.getSingleCategory(id);
        Log.d("Responce", categoriesEntity.getCategoryName());
        Intent intent = new Intent();
        intent.putExtra("CATEGORY", categoriesEntity.getCategoryName());
        intent.putExtra("CATEGORYID", categoriesEntity.getCategoryIcon());
        setResult(100, intent);
        finish();
    }
}
