package com.example.aftbhadk.myexpence.Models;

public class ExpenceModel {
    private int id;
    private String date;
    private String categoryID;
    private int categoryIcon;
    private String amount;

    public ExpenceModel(int id, String date, String categoryID, int categoryIcon, String amount) {
        this.id = id;
        this.date = date;
        this.categoryID = categoryID;
        this.categoryIcon = categoryIcon;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public int getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(int categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
