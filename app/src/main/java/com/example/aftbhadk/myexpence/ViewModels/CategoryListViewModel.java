package com.example.aftbhadk.myexpence.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.aftbhadk.myexpence.DataBase.AppDatabase;
import com.example.aftbhadk.myexpence.DataBase.Entity.CategoriesEntity;

import java.util.List;

public class CategoryListViewModel extends AndroidViewModel {

    private AppDatabase appDatabase;
    private LiveData<List<CategoriesEntity>> categoriesEntityList;


    public CategoryListViewModel(@NonNull Application application) {
        super(application);
        appDatabase = AppDatabase.getAppDatabase(this.getApplication());
        categoriesEntityList = appDatabase.categoryDao().getallCategories();
    }

    public LiveData<List<CategoriesEntity>> getCategoriesEntityList() {
        return categoriesEntityList;
    }

    public void setCategoriesEntityList(final CategoriesEntity... categoriesEntityList) {
        new addAsyncTask(appDatabase).execute(categoriesEntityList);
    }

    private static class addAsyncTask extends AsyncTask<CategoriesEntity, Void, Void> {

        private AppDatabase db;

        addAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final CategoriesEntity... params) {
            db.categoryDao().insertCategories(params[0]);
            return null;
        }

    }

    public CategoriesEntity getSingleCategory(int id) {
        CategoriesEntity categoriesEntity = appDatabase.categoryDao().getCategoryFromID(id);
        return categoriesEntity;
    }
}
