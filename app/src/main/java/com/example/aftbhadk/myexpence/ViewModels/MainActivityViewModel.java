package com.example.aftbhadk.myexpence.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.example.aftbhadk.myexpence.DataBase.AppDatabase;
import com.example.aftbhadk.myexpence.DataBase.Entity.CategoriesEntity;
import com.example.aftbhadk.myexpence.DataBase.Entity.ExpenceEntity;

import java.util.List;

public class MainActivityViewModel extends AndroidViewModel {
    private AppDatabase appDatabase;
    private LiveData<List<ExpenceEntity>> listLiveData;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        appDatabase = AppDatabase.getAppDatabase(this.getApplication());
        listLiveData = appDatabase.expenceDao().getAllExpences();
    }

    public LiveData<List<ExpenceEntity>> getListLiveData() {
        return listLiveData;
    }

    public void deleteSingleExpence(ExpenceEntity... expenceEntities) {
        new deleteAsyncTask(appDatabase).execute(expenceEntities);
    }

    public void setListLiveData(final ExpenceEntity... expenceEntities) {
        new addAsyncTask(appDatabase).execute(expenceEntities);
    }

    private static class addAsyncTask extends AsyncTask<ExpenceEntity, Void, Void> {

        private AppDatabase db;

        addAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final ExpenceEntity... params) {
            db.expenceDao().addExpence(params[0]);
            return null;
        }

    }

    private static class deleteAsyncTask extends AsyncTask<ExpenceEntity, Void, Void> {

        private AppDatabase db;

        deleteAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final ExpenceEntity... params) {
            db.expenceDao().deleteSingleExpence(params[0]);
            return null;
        }

    }
}
