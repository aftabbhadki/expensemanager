package com.example.aftbhadk.myexpence.DataBase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.aftbhadk.myexpence.DataBase.Dao.CategoryDao;
import com.example.aftbhadk.myexpence.DataBase.Dao.ExpenceDao;
import com.example.aftbhadk.myexpence.DataBase.Entity.CategoriesEntity;
import com.example.aftbhadk.myexpence.DataBase.Entity.ExpenceEntity;

@Database(entities = {CategoriesEntity.class, ExpenceEntity.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public static AppDatabase INSTANCE;

    public abstract CategoryDao categoryDao();
    public abstract ExpenceDao expenceDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "user-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}
