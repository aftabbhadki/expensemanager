package com.example.aftbhadk.myexpence.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.aftbhadk.myexpence.DataBase.Entity.CategoriesEntity;
import com.example.aftbhadk.myexpence.Interfaces.OnListItemClick;
import com.example.aftbhadk.myexpence.R;

import java.util.List;

public class CategoriesListAdapter extends RecyclerView.Adapter<CategoriesListAdapter.MyViewHolder> {

    Context context;
    List<CategoriesEntity> categoriesEntities;
    OnListItemClick onListItemClick;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.categorylist_row, parent, false);

        return new MyViewHolder(itemView);

    }

    public CategoriesListAdapter(Context context, List<CategoriesEntity> categoriesEntities, OnListItemClick onListItemClick) {
        this.context = context;
        this.categoriesEntities = categoriesEntities;
        this.onListItemClick = onListItemClick;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final CategoriesEntity categoriesEntity = categoriesEntities.get(position);
        holder.ctTitle.setText(categoriesEntity.getCategoryName());
        holder.ctIcon.setImageResource(categoriesEntity.getCategoryIcon());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onListItemClick.OnClickItem(position,categoriesEntity.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoriesEntities.size();
    }

    public void addItems(List<CategoriesEntity> categoriesEntities) {
        this.categoriesEntities = categoriesEntities;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView ctTitle;
        private ImageView ctIcon;
        private RelativeLayout container;

        public MyViewHolder(View itemView) {
            super(itemView);
            ctTitle = (TextView) itemView.findViewById(R.id.ctTitle);
            ctIcon = (ImageView) itemView.findViewById(R.id.ctIcon);
            container = (RelativeLayout) itemView.findViewById(R.id.container);
        }
    }
}
